Download
========

Release notes
-------------

Release notes for latest version:
https://projects.ow2.org/view/lemonldap-ng/lemonldap-ng-2-0-9-is-out

Go on https://projects.ow2.org/bin/view/lemonldap-ng/ for older
versions.

See also :doc:`upgrade notes<upgrade>`.

Packages and archives
---------------------

Stable version (2.0.9)
~~~~~~~~~~~~~~~~~~~~~~

Tarball
^^^^^^^

-  `Tarball <https://release.ow2.org/lemonldap/lemonldap-ng-2.0.9.tar.gz>`__

RPM
^^^


.. tip::

    You can:
    -  Use :ref:`our own YUM repository<installrpm-yum-repository>`.
    -  Download them here and :ref:`install pre-required packages<prereq-yum>`.


RHEL/CentOS 7
'''''''''''''

-  `RPM
   bundle <https://release.ow2.org/lemonldap/lemonldap-ng-2.0.9_el7.rpm.tar.gz>`__
-  `Source
   RPM <https://release.ow2.org/lemonldap/lemonldap-ng-2.0.9-1.el7.src.rpm>`__

RHEL/CentOS 8
'''''''''''''

-  `RPM
   bundle <https://release.ow2.org/lemonldap/lemonldap-ng-2.0.9_el8.rpm.tar.gz>`__
-  `Source
   RPM <https://release.ow2.org/lemonldap/lemonldap-ng-2.0.9-1.el8.src.rpm>`__

Debian
^^^^^^


.. tip::

    You can:

    -  Use
       :ref:`packages provided by Debian<installdeb-official-repository>`.
    -  Use
       :ref:`our own Debian repository<installdeb-llng-repository>`.
    -  Download them here and
       :ref:`install pre-required packages<prereq-apt-get>`.


-  `DEB
   bundle <https://release.ow2.org/lemonldap/lemonldap-ng-2.0.9_deb.tar.gz>`__

Docker
^^^^^^

See https://hub.docker.com/r/coudot/lemonldap-ng/

::

   docker pull coudot/lemonldap-ng

Nightly builds from master branch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debian repository of master branch, rebuilt every night:
http://lemonldap-ng.ow2.io/lemonldap-ng/

Older versions
~~~~~~~~~~~~~~

You can find all versions on `OW2
releases <https://release.ow2.org/lemonldap/>`__.

Contributions
-------------

See https://github.com/LemonLDAPNG

.. _download-getting-sources-from-svn-repository:

Git repository
--------------

See https://gitlab.ow2.org/lemonldap-ng/lemonldap-ng

::

   git clone git@gitlab.ow2.org:lemonldap-ng/lemonldap-ng.git
